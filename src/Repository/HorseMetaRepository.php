<?php

namespace App\Repository;

use App\Entity\HorseMeta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HorseMeta|null find($id, $lockMode = null, $lockVersion = null)
 * @method HorseMeta|null findOneBy(array $criteria, array $orderBy = null)
 * @method HorseMeta[]    findAll()
 * @method HorseMeta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HorseMetaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HorseMeta::class);
    }

    // /**
    //  * @return HorseMeta[] Returns an array of HorseMeta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HorseMeta
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
