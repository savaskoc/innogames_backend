<?php

namespace App\Repository;

use App\Entity\RaceMode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RaceMode|null find($id, $lockMode = null, $lockVersion = null)
 * @method RaceMode|null findOneBy(array $criteria, array $orderBy = null)
 * @method RaceMode[]    findAll()
 * @method RaceMode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RaceModeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RaceMode::class);
    }

    public function findWithHorseMeta($id): ?RaceMode
    {
        return $this->createQueryBuilder('r')
            ->select('r, m')
            ->join('r.horseMeta', 'm')
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return RaceMode[] Returns an array of RaceMode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RaceMode
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
