<?php

namespace App\Repository;

use App\Entity\Horse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Horse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Horse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Horse[]    findAll()
 * @method Horse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HorseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Horse::class);
        $this->getClassMetadata()->addNamedNativeQuery([
            'resultClass' => '__CLASS__',
            'name' => 'top_horses',
            'query' => 'SELECT * FROM (SELECT *, DENSE_RANK() OVER (PARTITION BY race_id ORDER BY ticks ASC, distance DESC) rank FROM horse) t WHERE race_id IN (:id) AND rank <= :limit'
        ]);
    }

    public function findTop(array $races, int $limit)
    {
        return $this->createNativeNamedQuery('top_horses')
            ->setParameter('id', $races)
            ->setParameter('limit', $limit)
            ->getResult();
    }

    public function findBest()
    {
        return $this->createQueryBuilder('h')
            ->where('h.ticks > 0 AND h.distance > 0')
            ->orderBy('h.ticks', 'ASC')
            ->addOrderBy('h.distance', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
