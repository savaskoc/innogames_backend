<?php

namespace App\Repository;

use App\Entity\Race;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Race|null find($id, $lockMode = null, $lockVersion = null)
 * @method Race|null findOneBy(array $criteria, array $orderBy = null)
 * @method Race[]    findAll()
 * @method Race[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Race::class);
    }

    public function findActiveWithStats()
    {
        return $this->createQueryBuilder('r')
            ->select('r.id, r.createdAt, r.isCompleted, MAX(h.distance) AS distance, MAX(h.ticks) AS ticks')
            ->join('r.horses', 'h')
            ->where('r.isCompleted = false')
            ->groupBy('r.id')
            ->getQuery()
            ->getResult();
    }

    public function findLatest($limit)
    {
        return $this->createQueryBuilder('r')
            ->where('r.isCompleted = true')
            ->orderBy('r.createdAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
