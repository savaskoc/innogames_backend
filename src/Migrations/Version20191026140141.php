<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191026140141 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE horse_meta (id INT AUTO_INCREMENT NOT NULL, speed_min NUMERIC(3, 1) NOT NULL, speed_max NUMERIC(3, 1) NOT NULL, strength_min NUMERIC(3, 1) NOT NULL, strength_max NUMERIC(3, 1) NOT NULL, endurance_min NUMERIC(3, 1) NOT NULL, endurance_max NUMERIC(3, 1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE race_mode (id INT AUTO_INCREMENT NOT NULL, horse_meta_id INT NOT NULL, distance INT NOT NULL, horse_count INT NOT NULL, base_speed NUMERIC(3, 1) NOT NULL, slow_speed NUMERIC(3, 1) NOT NULL, endurance_distance INT NOT NULL, strength_percentage NUMERIC(4, 1) NOT NULL, INDEX IDX_CAB757E484103567 (horse_meta_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE race_mode ADD CONSTRAINT FK_CAB757E484103567 FOREIGN KEY (horse_meta_id) REFERENCES horse_meta (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE race_mode DROP FOREIGN KEY FK_CAB757E484103567');
        $this->addSql('DROP TABLE horse_meta');
        $this->addSql('DROP TABLE race_mode');
    }
}
