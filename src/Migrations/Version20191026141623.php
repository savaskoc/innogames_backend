<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191026141623 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE horse (id INT AUTO_INCREMENT NOT NULL, race_id INT NOT NULL, speed NUMERIC(3, 1) NOT NULL, strength NUMERIC(3, 1) NOT NULL, endurance NUMERIC(3, 1) NOT NULL, distance DOUBLE PRECISION NOT NULL, ticks DOUBLE PRECISION NOT NULL, INDEX IDX_629A2F186E59D40D (race_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE race (id INT AUTO_INCREMENT NOT NULL, race_mode_id INT NOT NULL, created_at DATETIME NOT NULL, is_completed TINYINT(1) NOT NULL, INDEX IDX_DA6FBBAF6855503A (race_mode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE horse ADD CONSTRAINT FK_629A2F186E59D40D FOREIGN KEY (race_id) REFERENCES race (id)');
        $this->addSql('ALTER TABLE race ADD CONSTRAINT FK_DA6FBBAF6855503A FOREIGN KEY (race_mode_id) REFERENCES race_mode (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horse DROP FOREIGN KEY FK_629A2F186E59D40D');
        $this->addSql('DROP TABLE horse');
        $this->addSql('DROP TABLE race');
    }
}
