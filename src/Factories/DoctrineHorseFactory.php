<?php

namespace App\Factories;

use App\Adapters\DoctrineHorseAdapter;
use App\Entity\Horse;
use App\Entity\HorseMeta;
use App\Game\Factories\HorseFactoryInterface;
use App\Game\HorseInterface;

class DoctrineHorseFactory implements HorseFactoryInterface
{
    protected $horseMeta;

    public function __construct(HorseMeta $horseMeta)
    {
        $this->horseMeta = $horseMeta;
    }

    public function create(): HorseInterface
    {
        $speed = $this->horseMeta->getSpeed();
        $strength = $this->horseMeta->getStrength();
        $endurance = $this->horseMeta->getEndurance();
        $horse = (new Horse)->setSpeed($speed)->setStrength($strength)->setEndurance($endurance);
        return new DoctrineHorseAdapter($horse);
    }
}
