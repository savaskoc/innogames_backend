<?php

namespace App\Factories;

use App\Adapters\BaseAdapter;
use App\Adapters\DoctrineRaceAdapter;
use App\Entity\Race;
use App\Game\Factories\RaceFactoryInterface;
use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

class DoctrineRaceFactory implements RaceFactoryInterface
{
    /**
     * @param RaceModeInterface|BaseAdapter $raceMode
     * @return RaceInterface
     */
    public function create(RaceModeInterface $raceMode): RaceInterface
    {
        $race = (new Race())->setRaceMode($raceMode->getEntity());
        return new DoctrineRaceAdapter($race);
    }
}
