<?php

namespace App\Controller;

use App\Entity\Horse;
use App\Entity\Race;
use App\Repository\HorseRepository;
use App\Repository\RaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/stats", name="stats_")
 */
class StatController extends AbstractController
{
    const LAST_COUNT = 5;
    const TOP_HORSES = 3;

    protected $raceRepository;
    protected $horseRepository;

    public function __construct(RaceRepository $raceRepository, HorseRepository $horseRepository)
    {
        $this->raceRepository = $raceRepository;
        $this->horseRepository = $horseRepository;
    }

    /**
     * @Route("/latest", name="latest", methods={"GET"})
     */
    public function latest()
    {
        $races = $this->raceRepository->findLatest(static::LAST_COUNT);
        $horses = $this->horseRepository->findTop($races, static::TOP_HORSES);
        /** @var Race $race */
        foreach ($races as $race) {
            $raceHorses = array_values(array_filter($horses, function (Horse $horse) use ($race) {
                return $race->getId() === $horse->getRace()->getId();
            }));
            $race->setHorses($raceHorses);
        }

        return $this->json(compact('races'), 200, [], ['groups' => ['default', 'detail']]);
    }

    /**
     * @Route("/best", name="best", methods={"GET"})
     */
    public function best()
    {
        $horse = $this->horseRepository->findBest();
        return $this->json($horse, 200, [], ['groups' => ['default']]);
    }
}
