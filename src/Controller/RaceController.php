<?php

namespace App\Controller;

use App\Entity\Race;
use App\Game\Exceptions\AlreadyCompletedException;
use App\Game\Exceptions\NotAllowedException;
use App\Repository\RaceRepository;
use App\Services\RaceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/races", name="races_")
 */
class RaceController extends AbstractController
{
    protected $raceService;

    public function __construct(RaceService $raceService)
    {
        $this->raceService = $raceService;
    }

    /**
     * @Route("/active", name="active", methods={"GET"})
     */
    public function active(RaceRepository $raceRepository)
    {
        $races = $raceRepository->findActiveWithStats();
        return $this->json(compact('races'), 200, [], ['groups' => ['default']]);
    }

    /**
     * @Route("/{id}", name="details", methods={"GET"})
     */
    public function details(Race $race)
    {
        return $this->json($race, 200, [], ['groups' => ['default', 'detail']]);
    }

    /**
     * @Route("", name="create", methods={"POST"})
     */
    public function create()
    {
        try {
            $race = $this->raceService->create();
            return $this->json($race, 200, [], ['groups' => 'default']);
        } catch (NotAllowedException $ex) {
            return $this->json(['message' => $ex->getMessage()], 403);
        }
    }

    /**
     * @Route("/{id}/advance", name="advance", methods={"POST"})
     */
    public function advance(Race $race)
    {
        try {
            $this->raceService->advance($race);
            return $this->json($race, 200, [], ['groups' => ['default', 'detail']]);
        } catch (AlreadyCompletedException $ex) {
            return $this->json(['message' => $ex->getMessage()], 400);
        }
    }
}
