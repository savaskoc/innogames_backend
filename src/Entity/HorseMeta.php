<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HorseMetaRepository")
 */
class HorseMeta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $speedMin;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $speedMax;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $strengthMin;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $strengthMax;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $enduranceMin;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $enduranceMax;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RaceMode", mappedBy="horseMeta")
     */
    private $raceModes;

    public function __construct()
    {
        $this->raceModes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpeedMin(): ?string
    {
        return $this->speedMin;
    }

    public function setSpeedMin(string $speedMin): self
    {
        $this->speedMin = $speedMin;

        return $this;
    }

    public function getSpeedMax(): ?string
    {
        return $this->speedMax;
    }

    public function setSpeedMax(string $speedMax): self
    {
        $this->speedMax = $speedMax;

        return $this;
    }

    public function getStrengthMin(): ?string
    {
        return $this->strengthMin;
    }

    public function setStrengthMin(string $strengthMin): self
    {
        $this->strengthMin = $strengthMin;

        return $this;
    }

    public function getStrengthMax(): ?string
    {
        return $this->strengthMax;
    }

    public function setStrengthMax(string $strengthMax): self
    {
        $this->strengthMax = $strengthMax;

        return $this;
    }

    public function getEnduranceMin(): ?string
    {
        return $this->enduranceMin;
    }

    public function setEnduranceMin(string $enduranceMin): self
    {
        $this->enduranceMin = $enduranceMin;

        return $this;
    }

    public function getEnduranceMax(): ?string
    {
        return $this->enduranceMax;
    }

    public function setEnduranceMax(string $enduranceMax): self
    {
        $this->enduranceMax = $enduranceMax;

        return $this;
    }

    /**
     * @return Collection|RaceMode[]
     */
    public function getRaceModes(): Collection
    {
        return $this->raceModes;
    }

    public function addRaceMode(RaceMode $raceMode): self
    {
        if (!$this->raceModes->contains($raceMode)) {
            $this->raceModes[] = $raceMode;
            $raceMode->setHorseMeta($this);
        }

        return $this;
    }

    public function removeRaceMode(RaceMode $raceMode): self
    {
        if ($this->raceModes->contains($raceMode)) {
            $this->raceModes->removeElement($raceMode);
            // set the owning side to null (unless already changed)
            if ($raceMode->getHorseMeta() === $this) {
                $raceMode->setHorseMeta(null);
            }
        }

        return $this;
    }

    public function getSpeed()
    {
        return $this->getRandom($this->getSpeedMin(), $this->getSpeedMax());
    }

    public function getStrength()
    {
        return $this->getRandom($this->getStrengthMin(), $this->getStrengthMax());
    }

    public function getEndurance()
    {
        return $this->getRandom($this->getEnduranceMin(), $this->getEnduranceMax());
    }

    protected function getRandom($min, $max)
    {
        return mt_rand($min * 10, $max * 10) / 10;
    }
}
