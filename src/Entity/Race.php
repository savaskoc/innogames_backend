<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RaceRepository")
 */
class Race
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("default")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RaceMode", inversedBy="races")
     * @ORM\JoinColumn(nullable=false)
     */
    private $raceMode;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("default")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("default")
     */
    private $isCompleted;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Horse", mappedBy="race", orphanRemoval=true)
     * @Groups("detail")
     */
    private $horses;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->isCompleted = false;
        $this->horses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaceMode(): ?RaceMode
    {
        return $this->raceMode;
    }

    public function setRaceMode(?RaceMode $raceMode): self
    {
        $this->raceMode = $raceMode;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    /**
     * @return Collection|Horse[]
     */
    public function getHorses(): ?Collection
    {
        $order = Criteria::create()->orderBy(['ticks' => Criteria::ASC, 'distance' => Criteria::DESC]);
        return $this->horses->matching($order);
    }

    public function addHorse(Horse $horse): self
    {
        if (!$this->horses->contains($horse)) {
            $this->horses[] = $horse;
            $horse->setRace($this);
        }

        return $this;
    }

    public function removeHorse(Horse $horse): self
    {
        if ($this->horses->contains($horse)) {
            $this->horses->removeElement($horse);
            // set the owning side to null (unless already changed)
            if ($horse->getRace() === $this) {
                $horse->setRace(null);
            }
        }

        return $this;
    }

    public function setHorses(array $horses)
    {
        $this->horses = new ArrayCollection($horses);
    }
}
