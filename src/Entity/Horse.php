<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HorseRepository")
 */
class Horse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("default")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Race", inversedBy="horses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $race;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     * @Groups("default")
     */
    private $speed;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     * @Groups("default")
     */
    private $strength;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     * @Groups("default")
     */
    private $endurance;

    /**
     * @ORM\Column(type="float")
     * @Groups("default")
     */
    private $distance;

    /**
     * @ORM\Column(type="float")
     * @Groups("default")
     */
    private $ticks;

    public function __construct()
    {
        $this->distance = 0;
        $this->ticks = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRace(): ?Race
    {
        return $this->race;
    }

    public function setRace(?Race $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getSpeed(): ?string
    {
        return $this->speed;
    }

    public function setSpeed(string $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function getStrength(): ?string
    {
        return $this->strength;
    }

    public function setStrength(string $strength): self
    {
        $this->strength = $strength;

        return $this;
    }

    public function getEndurance(): ?string
    {
        return $this->endurance;
    }

    public function setEndurance(string $endurance): self
    {
        $this->endurance = $endurance;

        return $this;
    }

    public function getDistance(): float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getTicks(): float
    {
        return $this->ticks;
    }

    public function setTicks(float $ticks): self
    {
        $this->ticks = $ticks;

        return $this;
    }
}
