<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RaceModeRepository")
 */
class RaceMode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $distance;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HorseMeta", inversedBy="raceModes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $horseMeta;

    /**
     * @ORM\Column(type="integer")
     */
    private $horseCount;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $baseSpeed;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $slowSpeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $enduranceDistance;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=1)
     */
    private $strengthPercentage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Race", mappedBy="raceMode", orphanRemoval=true)
     */
    private $races;

    public function __construct()
    {
        $this->races = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistance(): ?int
    {
        return $this->distance;
    }

    public function setDistance(int $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getHorseMeta(): ?HorseMeta
    {
        return $this->horseMeta;
    }

    public function setHorseMeta(?HorseMeta $horseMeta): self
    {
        $this->horseMeta = $horseMeta;

        return $this;
    }

    public function getHorseCount(): ?int
    {
        return $this->horseCount;
    }

    public function setHorseCount(int $horseCount): self
    {
        $this->horseCount = $horseCount;

        return $this;
    }

    public function getBaseSpeed(): ?string
    {
        return $this->baseSpeed;
    }

    public function setBaseSpeed(string $baseSpeed): self
    {
        $this->baseSpeed = $baseSpeed;

        return $this;
    }

    public function getSlowSpeed(): ?string
    {
        return $this->slowSpeed;
    }

    public function setSlowSpeed(string $slowSpeed): self
    {
        $this->slowSpeed = $slowSpeed;

        return $this;
    }

    public function getEnduranceDistance(): ?int
    {
        return $this->enduranceDistance;
    }

    public function setEnduranceDistance(int $enduranceDistance): self
    {
        $this->enduranceDistance = $enduranceDistance;

        return $this;
    }

    public function getStrengthPercentage(): ?string
    {
        return $this->strengthPercentage;
    }

    public function setStrengthPercentage(string $strengthPercentage): self
    {
        $this->strengthPercentage = $strengthPercentage;

        return $this;
    }

    /**
     * @return Collection|Race[]
     */
    public function getRaces(): Collection
    {
        return $this->races;
    }

    public function addRace(Race $race): self
    {
        if (!$this->races->contains($race)) {
            $this->races[] = $race;
            $race->setRaceMode($this);
        }

        return $this;
    }

    public function removeRace(Race $race): self
    {
        if ($this->races->contains($race)) {
            $this->races->removeElement($race);
            // set the owning side to null (unless already changed)
            if ($race->getRaceMode() === $this) {
                $race->setRaceMode(null);
            }
        }

        return $this;
    }
}
