<?php

namespace App\Exceptions;

use RuntimeException;

class RaceAlreadyCompletedException extends RuntimeException
{
}
