<?php

namespace App\Adapters;

use App\Entity\Horse;
use App\Game\HorseInterface;

class DoctrineHorseAdapter extends BaseAdapter implements HorseInterface
{
    /**
     * @var Horse
     */
    protected $entity;

    public function getSpeed(): float
    {
        return $this->entity->getSpeed();
    }

    public function getStrength(): float
    {
        return $this->entity->getStrength();
    }

    public function getEndurance(): float
    {
        return $this->entity->getEndurance();
    }

    public function getDistance(): float
    {
        return $this->entity->getDistance();
    }

    public function getTicks(): float
    {
        return $this->entity->getTicks();
    }

    public function advance(float $newDistance, float $newTicks)
    {
        $this->entity->setDistance($newDistance);
        $this->entity->setTicks($newTicks);
    }
}
