<?php

namespace App\Adapters;

use App\Entity\RaceMode;
use App\Factories\DoctrineHorseFactory;
use App\Game\Factories\HorseFactoryInterface;
use App\Game\RaceModeInterface;
use Doctrine\Common\Collections\Criteria;

class DoctrineRaceModeAdapter extends BaseAdapter implements RaceModeInterface
{
    /**
     * @var RaceMode
     */
    protected $entity;

    public function getActiveRaceCount(): int
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('isCompleted', false));
        return $this->entity->getRaces()->matching($criteria)->count();
    }

    public function getHorseFactory(): HorseFactoryInterface
    {
        return new DoctrineHorseFactory($this->entity->getHorseMeta());
    }

    public function getHorseCount(): int
    {
        return $this->entity->getHorseCount();
    }

    public function getDistance(): int
    {
        return $this->entity->getDistance();
    }

    public function getBaseSpeed(): float
    {
        return $this->entity->getBaseSpeed();
    }

    public function getEnduranceDistance(): int
    {
        return $this->entity->getEnduranceDistance();
    }

    public function getStrengthPercentage(): float
    {
        return $this->entity->getStrengthPercentage();
    }

    public function getSlowSpeed(): float
    {
        return $this->entity->getSlowSpeed();
    }
}
