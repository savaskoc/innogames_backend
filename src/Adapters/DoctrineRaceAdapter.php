<?php

namespace App\Adapters;

use App\Entity\Horse;
use App\Entity\Race;
use App\Game\HorseInterface;
use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

class DoctrineRaceAdapter extends BaseAdapter implements RaceInterface
{
    /**
     * @var Race
     */
    protected $entity;

    /**
     * @param HorseInterface|BaseAdapter $horse
     */
    public function addHorse(HorseInterface $horse)
    {
        $this->entity->addHorse($horse->getEntity());
    }

    public function isCompleted(): bool
    {
        return $this->entity->getIsCompleted();
    }

    public function getRaceMode(): RaceModeInterface
    {
        return new DoctrineRaceModeAdapter($this->entity->getRaceMode());
    }

    /**
     * @return HorseInterface[]
     */
    public function getHorses(): iterable
    {
        return $this->entity->getHorses()->map(function (Horse $horse) {
            return new DoctrineHorseAdapter($horse);
        });
    }
}
