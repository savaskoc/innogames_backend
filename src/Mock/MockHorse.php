<?php

namespace App\Mock;

use App\Game\HorseInterface;

class MockHorse implements HorseInterface
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getSpeed(): float
    {
        return $this->data['speed'];
    }

    public function getStrength(): float
    {
        return $this->data['strength'];
    }

    public function getEndurance(): float
    {
        return $this->data['endurance'];
    }

    public function getDistance(): float
    {
        return $this->data['distance'];
    }

    public function getTicks(): float
    {
        return $this->data['ticks'];
    }

    public function advance(float $newDistance, float $newTicks)
    {
        $this->data['distance'] = $newDistance;
        $this->data['ticks'] = $newTicks;
    }
}
