<?php

namespace App\Mock;

use App\Game\Factories\HorseFactoryInterface;
use App\Game\HorseInterface;

class MockHorseFactory implements HorseFactoryInterface
{
    const STAT_MIN = 0;
    const STAT_MAX = 10;

    public function create(): HorseInterface
    {
        return new MockHorse([
            'speed' => $this->getRandomStat(),
            'strength' => $this->getRandomStat(),
            'endurance' => $this->getRandomStat(),
            'distance' => 0,
            'ticks' => 0
        ]);
    }

    protected function getRandomStat()
    {
        return mt_rand(static::STAT_MIN * 10, static::STAT_MAX * 10) / 10;
    }
}
