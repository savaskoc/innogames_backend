<?php

namespace App\Mock;

use App\Game\Factories\RaceFactoryInterface;
use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

class MockRaceFactory implements RaceFactoryInterface
{
    public function create(RaceModeInterface $raceMode): RaceInterface
    {
        return new MockRace($raceMode);
    }
}
