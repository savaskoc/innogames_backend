<?php

namespace App\Mock;

use App\Game\Factories\HorseFactoryInterface;
use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

class MockRaceMode implements RaceModeInterface
{
    const DISTANCE = 1500;
    const HORSE_COUNT = 8;
    const BASE_SPEED = 5;
    const ENDURANCE_DISTANCE = 100;
    const STRENGTH_PERCENTAGE = 8;
    const SLOW_SPEED = 5;

    protected $races;

    public function __construct(array $races = [])
    {
        $this->races = $races;
    }

    public function getActiveRaceCount(): int
    {
        $activeRaces = array_filter($this->races, function (RaceInterface $race) {
            return $race->isCompleted() === false;
        });
        return count($activeRaces);
    }

    public function getHorseFactory(): HorseFactoryInterface
    {
        return new MockHorseFactory;
    }

    public function getHorseCount(): int
    {
        return static::HORSE_COUNT;
    }

    public function getDistance(): int
    {
        return static::DISTANCE;
    }

    public function getBaseSpeed(): float
    {
        return static::BASE_SPEED;
    }

    public function getEnduranceDistance(): int
    {
        return static::ENDURANCE_DISTANCE;
    }

    public function getStrengthPercentage(): float
    {
        return static::STRENGTH_PERCENTAGE;
    }

    public function getSlowSpeed(): float
    {
        return static::SLOW_SPEED;
    }

    public function addRace(RaceInterface $race)
    {
        $this->races[] = $race;
    }
}
