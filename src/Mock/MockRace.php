<?php

namespace App\Mock;

use App\Game\HorseInterface;
use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

class MockRace implements RaceInterface
{
    protected $raceMode;
    protected $horses;

    public function __construct(MockRaceMode $raceMode, array $horses = [])
    {
        $this->raceMode = $raceMode;
        $this->horses = $horses;

        $raceMode->addRace($this);
    }

    public function addHorse(HorseInterface $horse)
    {
        $this->horses[] = $horse;
    }

    public function isCompleted(): bool
    {
        /** @var HorseInterface $horse */
        foreach ($this->horses as $horse) {
            if ($horse->getDistance() < $this->raceMode->getDistance()) {
                return false;
            }
        }
        return true;
    }

    public function getRaceMode(): RaceModeInterface
    {
        return $this->raceMode;
    }

    /**
     * @return HorseInterface[]
     */
    public function getHorses(): iterable
    {
        return $this->horses;
    }
}
