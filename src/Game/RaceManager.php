<?php

namespace App\Game;

use App\Game\Exceptions\AlreadyCompletedException;
use App\Game\Exceptions\NotAllowedException;
use App\Game\Factories\RaceFactoryInterface;

class RaceManager
{
    protected $maximumAllowed;
    protected $raceFactory;

    public function __construct(int $maximumAllowed, RaceFactoryInterface $raceFactory)
    {
        $this->maximumAllowed = $maximumAllowed;
        $this->raceFactory = $raceFactory;
    }

    public function create(RaceModeInterface $raceMode)
    {
        $count = $raceMode->getActiveRaceCount();
        if ($count >= $this->maximumAllowed) {
            throw new NotAllowedException("Maximum $this->maximumAllowed active games are allowed.");
        }

        $horseCount = $raceMode->getHorseCount();
        $horseFactory = $raceMode->getHorseFactory();
        $race = $this->raceFactory->create($raceMode);
        for ($i = 0; $i < $horseCount; $i++) {
            $horse = $horseFactory->create();
            $race->addHorse($horse);
        }
        return $race;
    }

    public function advance(RaceInterface $race, float $ticks): bool
    {
        if ($race->isCompleted()) {
            throw new AlreadyCompletedException('This game is already completed.');
        }

        $horses = $race->getHorses();
        $raceMode = $race->getRaceMode();
        $raceDistance = $raceMode->getDistance();
        $activeHorseCount = count($horses);
        foreach ($horses as $horse) {
            $horseDistance = $horse->getDistance();
            if ($horseDistance >= $raceDistance) {
                $activeHorseCount--;
                continue;
            }

            $speed = $raceMode->getBaseSpeed() + $horse->getSpeed();
            $enduranceDistance = $raceMode->getEnduranceDistance() * $horse->getEndurance();
            if ($horseDistance >= $enduranceDistance) {
                $multiplier = 1 - $raceMode->getStrengthPercentage() * $horse->getStrength() / 100;
                $speed = $speed - $raceMode->getSlowSpeed() * $multiplier;
            }
            $newDistance = round($horseDistance + $speed * $ticks, 2);
            $newTicks = round($horse->getTicks() + $ticks);
            $horse->advance($newDistance, $newTicks);

            if ($newDistance >= $raceDistance) {
                $activeHorseCount--;
            }
        }
        return $activeHorseCount === 0;
    }
}
