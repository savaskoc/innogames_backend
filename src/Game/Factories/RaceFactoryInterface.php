<?php

namespace App\Game\Factories;

use App\Game\RaceInterface;
use App\Game\RaceModeInterface;

interface RaceFactoryInterface
{
    public function create(RaceModeInterface $raceMode): RaceInterface;
}
