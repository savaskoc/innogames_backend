<?php

namespace App\Game\Factories;

use App\Game\HorseInterface;

interface HorseFactoryInterface
{
    public function create(): HorseInterface;
}
