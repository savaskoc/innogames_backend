<?php

namespace App\Game;

use App\Game\Factories\HorseFactoryInterface;

interface RaceModeInterface
{
    public function getActiveRaceCount(): int;

    public function getHorseFactory(): HorseFactoryInterface;

    public function getHorseCount(): int;

    public function getDistance(): int;

    public function getBaseSpeed(): float;

    public function getEnduranceDistance(): int;

    public function getStrengthPercentage(): float;

    public function getSlowSpeed(): float;
}
