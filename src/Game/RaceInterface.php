<?php

namespace App\Game;

interface RaceInterface
{
    public function addHorse(HorseInterface $horse);

    public function isCompleted(): bool;

    public function getRaceMode(): RaceModeInterface;

    /**
     * @return HorseInterface[]
     */
    public function getHorses(): iterable;
}
