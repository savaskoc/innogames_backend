<?php

namespace App\Game\Exceptions;

use RuntimeException;

class AlreadyCompletedException extends RuntimeException
{
}
