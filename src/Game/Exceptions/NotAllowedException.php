<?php

namespace App\Game\Exceptions;

use RuntimeException;

class NotAllowedException extends RuntimeException
{
}
