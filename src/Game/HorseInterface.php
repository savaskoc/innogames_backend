<?php

namespace App\Game;

interface HorseInterface
{
    public function getSpeed(): float;

    public function getStrength(): float;

    public function getEndurance(): float;

    public function getDistance(): float;

    public function getTicks(): float;

    public function advance(float $newDistance, float $newTicks);
}
