<?php

namespace App\DataFixtures;

use App\Entity\HorseMeta;
use App\Entity\RaceMode;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $meta = (new HorseMeta)
            ->setSpeedMin(0)
            ->setSpeedMax(10)
            ->setStrengthMin(0)
            ->setStrengthMax(10)
            ->setEnduranceMin(0)
            ->setEnduranceMax(10);
        $manager->persist($meta);

        $mode = (new RaceMode)
            ->setDistance(1500)
            ->setHorseMeta($meta)
            ->setHorseCount(8)
            ->setBaseSpeed(5)
            ->setSlowSpeed(5)
            ->setEnduranceDistance(100)
            ->setStrengthPercentage(8);
        $manager->persist($mode);

        $manager->flush();
    }
}
