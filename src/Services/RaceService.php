<?php

namespace App\Services;

use App\Adapters\BaseAdapter;
use App\Adapters\DoctrineRaceAdapter;
use App\Adapters\DoctrineRaceModeAdapter;
use App\Entity\Race;
use App\Game\RaceInterface;
use App\Game\RaceManager;
use App\Repository\RaceModeRepository;
use Doctrine\ORM\EntityManagerInterface;

class RaceService
{
    const RACE_MODE = 1;
    const ADVANCE_TICKS = 10;

    protected $raceModeRepository;
    protected $entityManager;
    protected $raceManager;

    public function __construct(RaceModeRepository $raceModeRepository, EntityManagerInterface $entityManager,
                                RaceManager $raceManager)
    {
        $this->raceModeRepository = $raceModeRepository;
        $this->entityManager = $entityManager;
        $this->raceManager = $raceManager;
    }

    public function create(): Race
    {
        $raceMode = $this->raceModeRepository->find(static::RACE_MODE);
        $raceModeAdapter = new DoctrineRaceModeAdapter($raceMode);
        /** @var RaceInterface|BaseAdapter $race */
        $race = $this->raceManager->create($raceModeAdapter);
        /** @var Race $raceEntity */
        $raceEntity = $race->getEntity();
        $this->entityManager->transactional(function () use ($raceEntity) {
            $this->entityManager->persist($raceEntity);
            foreach ($raceEntity->getHorses() as $horse) {
                $this->entityManager->persist($horse);
            }
        });
        return $raceEntity;
    }

    public function advance(Race $race)
    {
        $this->entityManager->transactional(function () use ($race) {
            $raceAdapter = new DoctrineRaceAdapter($race);
            $isCompleted = $this->raceManager->advance($raceAdapter, static::ADVANCE_TICKS);
            $race->setIsCompleted($isCompleted);
        });
    }
}
