<?php

namespace App\Tests\Game;

use App\Game\Exceptions\NotAllowedException;
use App\Game\RaceManager;
use App\Game\RaceModeInterface;
use App\Mock\MockRace;
use App\Mock\MockRaceFactory;
use App\Mock\MockRaceMode;
use PHPUnit\Framework\TestCase;

class RaceManagerTest extends TestCase
{
    const TICKS = 10;

    /**
     * @dataProvider raceManagerProvider
     */
    public function testCreateRace(RaceManager $raceManager, RaceModeInterface $raceMode)
    {
        $race = $raceManager->create($raceMode);

        $this->assertInstanceOf(MockRace::class, $race);
        $this->assertEquals(MockRaceMode::HORSE_COUNT, count($race->getHorses()));
        $this->assertEquals(false, $race->isCompleted());

        $raceManager->create($raceMode);
        $raceManager->create($raceMode);

        $this->expectException(NotAllowedException::class);
        $raceManager->create($raceMode);
    }

    /**
     * @dataProvider raceManagerProvider
     */
    public function testAdvanceRace(RaceManager $raceManager, RaceModeInterface $raceMode)
    {
        $oldDistances = [];
        $oldTicks = [];
        $race = $raceManager->create($raceMode);
        do {
            $isCompleted = $raceManager->advance($race, static::TICKS);
            foreach ($race->getHorses() as $i => $horse) {
                $horseDistance = $oldDistances[$i] ?? 0;
                if ($horseDistance >= $raceMode->getDistance()) {
                    continue;
                }

                $speed = $raceMode->getBaseSpeed() + $horse->getSpeed();
                $enduranceDistance = $raceMode->getEnduranceDistance() * $horse->getEndurance();
                if ($horseDistance >= $enduranceDistance) {
                    $multiplier = 1 - $raceMode->getStrengthPercentage() * $horse->getStrength() / 100;
                    $speed = $speed - $raceMode->getSlowSpeed() * $multiplier;
                }

                $newDistance = $horseDistance + $speed * static::TICKS;
                $newTime = ($oldTicks[$i] ?? 0) + static::TICKS;

                $this->assertEquals($newDistance, $horse->getDistance());
                $this->assertEquals($newTime, $horse->getTicks());

                $oldDistances[$i] = $newDistance;
                $oldTicks[$i] = $newTime;
            }
        } while (!$isCompleted);
    }

    public function raceManagerProvider()
    {
        $raceManager = new RaceManager(3, new MockRaceFactory());
        $raceMode = new MockRaceMode();
        yield [$raceManager, $raceMode];
    }
}
