# API for Horse Race Simulator

This is a symfony micro app that contains game logic

## Environment Variables

You can see environment variables from .env file. APP_SECRET and DATABASE_URL values can be taken from 
[docker-compose repository](https://bitbucket.org/savaskoc/innogames_docker).

## Docker Compose

Also you can use [docker-compose repository](https://bitbucket.org/savaskoc/innogames_docker) for quick testing.

## After Install

You need to execute migrations and load fixtures after install.

`php bin/console doctrine:migrations:migrate`

`php bin/console doctrine:fixtures:load`
